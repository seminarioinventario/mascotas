// To parse this JSON data, do
//
//     final petsModel = petsModelFromJson(jsonString);

import 'dart:convert';

PetsModel petsModelFromJson(String str) => PetsModel.fromJson(json.decode(str));

String petsModelToJson(PetsModel data) => json.encode(data.toJson());

class PetsModel {
    String name;
    String raza;
    String especie;
    String edad;
    String estatura;
    String color;
    String estado;
    String comentarios;
    String imagen;
    String userId;

    PetsModel({
        this.name = '',
        this.raza = '',
        this.especie = '',
        this.edad = '',
        this.estatura = '',
        this.color = '',
        this.estado = '',
        this.comentarios = '',
        this.imagen,
        this.userId,
    });

    factory PetsModel.fromJson(Map<String, dynamic> json) => PetsModel(
        name        : json["name"],
        raza        : json["raza"],
        especie     : json["especie"],
        edad        : json["edad"],
        estatura    : json["estatura"],
        color       : json["color"],
        estado      : json["estado"],
        comentarios : json["comentarios"],
        imagen      : json["imagen"],
        userId      : json["userId"],
    );

    Map<String, dynamic> toJson() => {
        "name"        : name,
        "raza"        : raza,
        "especie"     : especie,
        "edad"        : edad,
        "estatura"    : estatura,
        "color"       : color,
        "estado"      : estado,
        "comentarios" : comentarios,
        "imagen"      : imagen,
        "userId"      : userId,
    };
}
