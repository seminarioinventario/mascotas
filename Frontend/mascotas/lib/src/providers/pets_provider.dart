import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mascotas/src/models/pets_model.dart';
import 'package:mascotas/src/secure_storage/secure_storage.dart';
import 'package:mime_type/mime_type.dart';

class PetsProvider {

  final _storage = new SessionStorage();
  final String _url = 'https://seminariouniajc.herokuapp.com';

  Future<bool> createPets( PetsModel pet ) async{

    final url = '$_url/pets/add';
    final token = await _storage.getValueforKey('token');

    final resp = await http.post(
      url,
      headers: {
        'content-type' : 'application/json',
        'authorization' : token
      },
      body: petsModelToJson(pet), 
    );

    final decodedData = json.decode(resp.body);
    print(decodedData);

    return true;

  }

  Future<bool> takePets( PetsModel pet ) async {

    final url = '$_url/pets/takePet';
    final token = await _storage.getValueforKey('token');
    final body = {
      "userId" : await _storage.getValueforKey('userId'),
      "petName" : pet.name
    };

    final resp = await http.post(
      url,
      headers: {
        'content-type' : 'application/json',
        'authorization' : token
      },
      body: json.encode(body), 
    );

    final decodedData = json.decode(resp.body);
    print(decodedData);

    return true;

  }

  Future<List<PetsModel>> loadPets() async {
    
    final url = '$_url/pets/';
    final token = await _storage.getValueforKey('token');

    final resp = await http.get(
      url,
      headers: {
        'content-type' : 'application/json',
        'authorization' : token
      }
    );

    final Map<String,dynamic> decodedData = json.decode(resp.body);
    final List<PetsModel> pets = new List();

    if ( decodedData['Data'] == null ) return [];

    decodedData['Data'].forEach( (pet) {
      final petTemp = PetsModel.fromJson(pet);
      pets.add(petTemp);
    });

    return pets;

  }

  Future<List<PetsModel>> loadMyPets() async {
    
    final token = await _storage.getValueforKey('token');
    final userID = await _storage.getValueforKey('userId');
    final url = '$_url/pets/findByUser/$userID';

    final resp = await http.get(
      url,
      headers: {
        'content-type' : 'text',
        'authorization' : token
      }
    );

    final Map<String,dynamic> decodedData = json.decode(resp.body);
    final List<PetsModel> pets = new List();

    if ( decodedData['Data'] == null ) return [];

    decodedData['Data'].forEach( (pet) {
      final petTemp = PetsModel.fromJson(pet);
      pets.add(petTemp);
    });

    return pets;

  }

  Future<String> uploadImage( File image ) async {

    final url = Uri.parse('https://api.cloudinary.com/v1_1/dyfwbsnyy/image/upload?upload_preset=k8l0jvcb');
    final mimeType = mime(image.path).split('/');

    final imageUploadRequest = http.MultipartRequest(
      'POST',
      url
    );

    final file = await http.MultipartFile.fromPath(
      'file', 
      image.path,
      contentType: MediaType( mimeType[0], mimeType[1] ) 
    );

    imageUploadRequest.files.add(file);

    final streamResponse = await imageUploadRequest.send();
    final resp = await http.Response.fromStream(streamResponse);

    if ( resp.statusCode != 200 && resp.statusCode != 201 ) {
      print('salio mal');
      print(resp.body);
      return null;
    }

    final respData = json.decode(resp.body);
    print(respData);

    return respData['secure_url'];

  }

}