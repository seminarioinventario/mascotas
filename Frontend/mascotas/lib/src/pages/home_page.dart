import 'package:flutter/material.dart';
import 'package:mascotas/src/secure_storage/secure_storage.dart';

class HomePage extends StatelessWidget {

 @override
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      title: const Text('Menu Principal'),
      actions: <Widget>[
        IconButton(
          icon: Icon( Icons.exit_to_app, color: Colors.white ),
          onPressed: () async{
            final _session = new SessionStorage();
            await _session.deleteAllValues();
            Navigator.pushReplacementNamed(context, 'login');
          },
        )
      ],
    ),
    body: Stack(
        children: <Widget>[
          _homeForm(context)
        ],
      ),
  );
  drawer: Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: const <Widget>[
        DrawerHeader(
          decoration: BoxDecoration(
            color: Colors.blue,
          ),
          child: Text(
            'Drawer Header',
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
        ),
        ListTile(
          leading: Icon(Icons.message),
          title: Text('Messages'),
        ),
        ListTile(
          leading: Icon(Icons.account_circle),
          title: Text('Profile'),
        ),
        ListTile(
          leading: Icon(Icons.settings),
          title: Text('Settings'),
        ),
      ],
    ),
  );
}

   Widget _homeForm(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[

          SafeArea(
            child: Container(
              height: 100.0,
            ),
          ),

          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric( vertical: 30.0 ),
            padding: EdgeInsets.symmetric( vertical: 50.0 ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 3.0,
                  offset: Offset(0.0, 5.0),
                  spreadRadius: 3.0
                )
              ]
            ),
            child: Column(
              children: <Widget>[
                Text('Selecciona una opción', style: TextStyle(fontSize: 20.0),),
                SizedBox( height: 60.0 ),
                _createMascotas(),
                SizedBox( height: 30.0 ),
                _createAdopcion(),
                SizedBox( height: 30.0 ),
               // _createButton(bloc)
              ],
            ),
          ),
          SizedBox( height: 100.0 )
        ],
      ),
    );
   }


  Widget _createMascotas (){
       return StreamBuilder(
       builder: (BuildContext context, AsyncSnapshot snapshot){
        return RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
            child: Text('Mascotas')
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0)
          ),
          elevation: 0.0,
          color: Colors.blueAccent,
          textColor: Colors.white,
          onPressed: () {Navigator.pushReplacementNamed(context, 'mascotas');}
        );
      },
    );
  }
  Widget _createAdopcion (){

       return StreamBuilder(
       builder: (BuildContext context, AsyncSnapshot snapshot){
        return RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
            child: Text('Mis Mascotas'), 
             
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0)
          ),
          elevation: 0.0,
          color: Colors.blueAccent,
          textColor: Colors.white,
          onPressed: () {Navigator.pushReplacementNamed(context, 'adopcion');}
        );
      },
    );
  }

}