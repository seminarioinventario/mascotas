import 'package:flutter/material.dart';
import 'package:mascotas/src/bloc/provider.dart';
import 'package:mascotas/src/providers/user_provider.dart';
import 'package:mascotas/src/secure_storage/secure_storage.dart';
import 'package:mascotas/src/utils/utils.dart';

class LoginPage extends StatelessWidget {

  final userProvider = new UserProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _createFondo(context),
          _loginForm(context),
        ],
      )
    );
  }


  Widget _loginForm(BuildContext context) {

    final bloc = Provider.of(context);
    final size = MediaQuery.of(context).size;

    _checkToken(context);

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[

          SafeArea(
            child: Container(
              height: 180.0,
            ),
          ),

          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric( vertical: 30.0 ),
            padding: EdgeInsets.symmetric( vertical: 50.0 ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 3.0,
                  offset: Offset(0.0, 5.0),
                  spreadRadius: 3.0
                )
              ]
            ),
            child: Column(
              children: <Widget>[
                Text('Inicio de sesión', style: TextStyle(fontSize: 20.0),),
                SizedBox( height: 60.0 ),
                _createEmail(bloc),
                SizedBox( height: 30.0 ),
                _createPassword(bloc),
                SizedBox( height: 30.0 ),
                _createButton(bloc)
              ],
            ),
          ),
          FlatButton(
            onPressed: () => Navigator.pushReplacementNamed(context, 'register'), 
            child: Text('Crear una nueva cuenta')
          ),
          SizedBox( height: 100.0 )
        ],
      ),
    );
  }

  void _checkToken(BuildContext context) async{
    final _storage = new SessionStorage();
    final values = await _storage.getAllValues();
    if ( values.isNotEmpty ) {
      print('No way');
      Navigator.pushReplacementNamed(context, 'home');
    }
  }

  Widget _createEmail( LoginBloc bloc ) {

    return StreamBuilder(
      stream: bloc.emailStream ,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              icon: Icon( Icons.alternate_email, color: Colors.blueAccent ),
              hintText: 'ejemplo@correo.com',
              labelText: 'Correo electrónico',
              counterText: snapshot.data,
              errorText: snapshot.error
            ),
            onChanged: bloc.changeEmail,
          ),
        );
      },
    );

  }

  Widget _createPassword( LoginBloc bloc ) {

    return StreamBuilder(
      stream: bloc.passwordStream ,
      builder: (BuildContext context, AsyncSnapshot snapshot){
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: TextField(
          obscureText: true,
          decoration: InputDecoration(
            icon: Icon( Icons.lock_outline, color: Colors.blueAccent ),
            labelText: 'Contraseña',
            errorText: snapshot.error
          ),
          onChanged: bloc.changePassword,
        ),
      );
      },
    );

  }

  Widget _createButton(LoginBloc bloc) {

    return StreamBuilder(
      stream: bloc.formValidStream ,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
            child: Text('Ingresar')
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0)
          ),
          elevation: 0.0,
          color: Colors.blueAccent,
          textColor: Colors.white,
          onPressed: snapshot.hasData ? ()=> _login(bloc, context) : null,
        );
      },
    );

  }

  _login(LoginBloc bloc, BuildContext context) async {

    Map info = await userProvider.login(bloc.email, bloc.password);

    if ( info['ok'] ) {
      Navigator.pushReplacementNamed(context, 'home');
    }
    else {
      showAlert( context, info['message'] );
    }

  }

  Widget _createFondo(BuildContext context) {

    final size = MediaQuery.of(context).size;

    final background = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors : <Color> [
            Color.fromRGBO(51, 97, 255, 1.0),
            Color.fromRGBO(51, 97, 255, 1.0)
          ]
        )
      ),
    );

    return Stack(
      children: <Widget>[
        background,

        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Icon( Icons.person_pin_circle, color : Colors.white, size: 100.0 ),
              SizedBox( height: 10.0, width: double.infinity, ),
              Text('Mascotas', style: TextStyle( color: Colors.white, fontSize: 25.0 ))
            ],
          ),
        )
      ],
    );
  }
}