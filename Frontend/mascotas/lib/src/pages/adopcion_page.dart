import 'package:flutter/material.dart';
import 'package:mascotas/src/bloc/provider.dart';
import 'package:mascotas/src/models/pets_model.dart';

class AdopcionPage extends StatelessWidget {

 @override
  Widget build(BuildContext context) {

    final petsBloc = Provider.petsBloc(context);
    petsBloc.loadMyPets();

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {Navigator.pushReplacementNamed(context, 'home'); },
        ),  
        title: const Text('Mis Mascotas'),
        
      ),
      body: _createList(petsBloc)
    );
  }

  Widget _createList(PetsBloc petsBloc) {

    return StreamBuilder(
      stream: petsBloc.petsStream ,
      builder: (BuildContext context, AsyncSnapshot<List<PetsModel>> snapshot){
        if ( snapshot.hasData ) {

          final pets = snapshot.data;
          return ListView.builder(
            itemCount: pets.length,
            itemBuilder: ( context, i ) => _createItem( context, pets[i] ),
          );

        }
        else {
          return Center( child: CircularProgressIndicator() );
        }
      },
    );

  }

  Widget _createItem(BuildContext context, PetsModel pet ) {

    return Card(
      child: Column(
        children: <Widget>[

          ( pet.imagen == null ) 
            ? Image(image: AssetImage('assets/no-image.png'))
            : FadeInImage(
              placeholder: AssetImage('assets/jar-loading.gif'), 
              image: NetworkImage( pet.imagen ),
              height: 300.0,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ListTile(
            title: Text( '${pet.name} - ${pet.especie}' ),
            subtitle: Text( pet.comentarios ),
            onTap: (){},
          )
        ],
      ),
    );

  }

}