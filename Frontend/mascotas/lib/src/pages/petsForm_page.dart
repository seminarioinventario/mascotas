import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mascotas/src/bloc/provider.dart';
import 'package:mascotas/src/models/pets_model.dart';
import 'package:mascotas/src/secure_storage/secure_storage.dart';
import 'package:mascotas/src/utils/utils.dart' as utils;
 
class PetsFormPage extends StatefulWidget {

  @override
  _PetsFormPageState createState() => _PetsFormPageState();
}

class _PetsFormPageState extends State<PetsFormPage> {

  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  PetsBloc petsBloc;
  PetsModel pets = new PetsModel();
  bool _saving = false;

  File photo;

  @override
  Widget build(BuildContext context) {

    petsBloc = Provider.petsBloc(context);
    
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Registrar Mascota'),
        actions: <Widget>[
          IconButton(
            icon: Icon( Icons.photo_size_select_actual ), 
            onPressed: _selectPhoto,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                _showPhoto(),
                _createName(),
                _createEspecie(),
                _createRaza(),
                _createEdad(),
                _createEstatura(),
                _createColor(),
                _createComentarios(),
                SizedBox( height: 30.0 ),
                _createButton()
              ],
            ),
          ),
        ),
      ),
    );

  }

  Widget _createName(  ) {

    return TextFormField(
      initialValue: pets.name,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: 'Nombre'
      ),
      onSaved: (value) => pets.name = value,
      validator: (value) {
        if ( value.length <=0 ) {
          return 'Ingrese información en el campo';
        }
        else {
          return null;
        }
      },
    );

  }

  Widget _createRaza( ) {

    return TextFormField(
      initialValue: pets.raza,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: 'Raza'
      ),
      onSaved: (value) => pets.raza = value,
      validator: (value) {
        if ( value.length <=0 ) {
          return 'Ingrese información en el campo';
        }
        else {
          return null;
        }
      },
    );

  }

  Widget _createEspecie( ) {

    return TextFormField(
      initialValue: pets.especie,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: 'Especie'
      ),
      onSaved: (value) => pets.especie = value,
      validator: (value) {
        if ( value.length <=0 ) {
          return 'Ingrese información en el campo';
        }
        else {
          return null;
        }
      },
    );

  }

  Widget _createEdad( ) {

    return TextFormField(
      initialValue: pets.edad,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        labelText: 'Edad'
      ),
      onSaved: (value) => pets.edad = value,
      validator: (value) {
        if( utils.isNumeric(value) ) {
          return null;
        }
        else {
          return 'Solo se permiten números';
        }
      },
    );

  }

  Widget _createEstatura( ) {

    return TextFormField(
      initialValue: pets.estatura,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        labelText: 'Estatura'
      ),
      onSaved: (value) => pets.estatura = value,
      validator: (value) {
        if( utils.isNumeric(value) ) {
          return null;
        }
        else {
          return 'Solo se permiten números';
        }
      },
    );

  }

  Widget _createColor( ) {

    return TextFormField(
      initialValue: pets.color,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: 'Color'
      ),
      onSaved: (value) => pets.color = value,
      validator: (value) {
        if ( value.length <=0 ) {
          return 'Ingrese información en el campo';
        }
        else {
          return null;
        }
      },
    );

  }

  Widget _createComentarios( ) {

    return TextFormField(
      initialValue: pets.comentarios,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: 'Comentarios'
      ),
      onSaved: (value) => pets.comentarios = value,
      validator: (value) {
        if ( value.length <=0 ) {
          return 'Ingrese información en el campo';
        }
        else {
          return null;
        }
      },
    );

  }

  Widget _createButton() {

    return RaisedButton(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
        child: Text('Registrar')
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0)
      ),
      elevation: 0.0,
      color: Colors.blueAccent,
      textColor: Colors.white,
      onPressed: ( _saving ) ? null : _submit
    );
  }

  void _submit() async {
    final _storage = new SessionStorage();

    if ( !formKey.currentState.validate() ) return;
  
    formKey.currentState.save();

    pets.estado = '2';
    pets.userId = await _storage.getValueforKey('userId');
    print(pets.toJson());

    setState(() {
      _saving = true;
    });

    if ( photo != null ) {
      pets.imagen = await petsBloc.uploadPhoto(photo);
    }
    
    await petsBloc.insertPets(pets);
    
    setState(() {
      _saving = false;
    });
    showSnackbar('Mascota registrada');

    Navigator.pop(context,() {
      setState(() {});
    });

  }

  void showSnackbar(String message) {
    final snackbar = SnackBar(
      content: Text( message ),
      duration: Duration( milliseconds: 3000 ),
    );

    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  _showPhoto() {

    if( pets.imagen != null ) {

      return FadeInImage(
        placeholder: AssetImage('assets/jar-loading.gif'), 
        image: NetworkImage(pets.imagen),
        height: 300.0,
        fit: BoxFit.contain,
      );
    }
    else {

      return Image(
        image: AssetImage(  photo?.path ?? 'assets/no-image.png'),
        height: 300.0,
        fit: BoxFit.cover,
      );

    }

  }

  _selectPhoto() async {

    photo = await ImagePicker.pickImage(
      source: ImageSource.gallery
    );

    if ( photo != null ) {
      pets.imagen = null;
    }

    setState(() {});

  }


}