import 'dart:io';

import 'package:mascotas/src/models/pets_model.dart';
import 'package:mascotas/src/providers/pets_provider.dart';
import 'package:rxdart/subjects.dart';

class PetsBloc {

  final _petsController = new BehaviorSubject<List<PetsModel>>();
  final _loadingController = new BehaviorSubject<bool>();

  final _petsProvider = new PetsProvider();

  Stream<List<PetsModel>> get petsStream => _petsController.stream;
  Stream<bool> get loading => _loadingController.stream;


  void loadPets() async{

    final pets = await _petsProvider.loadPets();
    _petsController.sink.add( pets );

  }

  void loadMyPets() async{

    final pets = await _petsProvider.loadMyPets();
    _petsController.sink.add( pets );

  }

  void insertPets( PetsModel pet ) async {

    _loadingController.sink.add(true);
    await _petsProvider.createPets(pet);
    _loadingController.sink.add(false);

  }

  void takePets( PetsModel pet ) async {

    _loadingController.sink.add(true);
    await _petsProvider.takePets(pet);
    _loadingController.sink.add(false);

  }

  Future<String> uploadPhoto( File photo ) async {

    _loadingController.sink.add(true);
    final photoUrl = _petsProvider.uploadImage(photo);
    _loadingController.sink.add(false);

    return photoUrl;

  }


  dispose() {
    _petsController?.close();
    _loadingController?.close();
  }

}