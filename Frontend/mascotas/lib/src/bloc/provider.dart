import 'package:flutter/material.dart';

import 'package:mascotas/src/bloc/pets_bloc.dart';
export 'package:mascotas/src/bloc/pets_bloc.dart';

import 'register_bloc.dart';
export 'register_bloc.dart';

import 'login_bloc.dart';
export 'login_bloc.dart';


class Provider extends InheritedWidget {

  final loginBloc = new LoginBloc();
  final registerBloc = new RegisterBloc();
  final _petsBloc = new PetsBloc();

  static Provider _instance;

  factory Provider({ Key key, Widget child }) {
    if ( _instance == null ) {
      _instance = new Provider._internal(key: key, child: child);
    }

    return _instance;
  }

  Provider._internal({ Key key, Widget child })
    : super(key: key, child: child);


  //Provider({ Key key, Widget child })
  //  : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static LoginBloc of ( BuildContext context ) {
    return (context.dependOnInheritedWidgetOfExactType<Provider>()).loginBloc;
  }

  static RegisterBloc register ( BuildContext context ) {
    return (context.dependOnInheritedWidgetOfExactType<Provider>()).registerBloc;
  }

  static PetsBloc petsBloc ( BuildContext context ) {
    return (context.dependOnInheritedWidgetOfExactType<Provider>())._petsBloc;
  }

}