

import 'dart:async';

class Validators {


  final validateEmail = StreamTransformer<String, String>.fromHandlers(
    handleData: ( email, sink ) {

      Pattern pattern = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)";
      RegExp regExp   = new RegExp(pattern);

      if ( regExp.hasMatch(email) ) {
        sink.add(email);
      }
      else {
        sink.addError('El correo electrónico no es valido');
      }

    }
  );


  final validatePassword = StreamTransformer<String, String>.fromHandlers(
    handleData: ( password, sink ) {

      if ( password.length >= 5 ) {
        sink.add( password );
      }
      else {
        sink.addError( 'Debe de contener más de 6 caracteres' );
      }

    }
  );

}