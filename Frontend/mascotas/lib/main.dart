import 'package:flutter/material.dart';
import 'package:mascotas/src/bloc/provider.dart';
import 'package:mascotas/src/pages/adopcion_page.dart';
import 'package:mascotas/src/pages/home_page.dart';
import 'package:mascotas/src/pages/login_page.dart';
import 'package:mascotas/src/pages/mascotas_page.dart';
import 'package:mascotas/src/pages/petsForm_page.dart';
import 'package:mascotas/src/pages/register_page.dart';
 
void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());

}
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: 'login',
        routes: {
          'login'     : ( BuildContext context ) => LoginPage(),
          'register'  : ( BuildContext context ) => RegisterPage(),
          'home'      : ( BuildContext context ) => HomePage(),
          'adopcion'  : ( BuildContext context ) => AdopcionPage(),
          'mascotas'  : ( BuildContext context ) => MascotasPage(),
          'petsForm'  : ( BuildContext context ) => PetsFormPage()
        },
        theme: ThemeData(
          primaryColor: Colors.blueAccent
        ),
      ),
    );
  }
}