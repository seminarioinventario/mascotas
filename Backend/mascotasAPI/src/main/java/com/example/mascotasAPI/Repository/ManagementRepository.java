package com.example.mascotasAPI.Repository;

import com.example.mascotasAPI.Model.Management;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ManagementRepository extends MongoRepository<Management,String> {
    Management findByid(String id);
    List<Management> findAll();
    List<Management> findAllByPetId(String petId);
    List<Management> findAllByUserId(String userId);
}
