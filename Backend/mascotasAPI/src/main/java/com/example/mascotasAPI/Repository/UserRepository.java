package com.example.mascotasAPI.Repository;

import com.example.mascotasAPI.Model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User,String> {
    User findByEmail(String email);
    List<User> findAllByOrderByName();
}
