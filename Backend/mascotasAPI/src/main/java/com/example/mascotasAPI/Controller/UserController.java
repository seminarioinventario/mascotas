package com.example.mascotasAPI.Controller;

import com.example.mascotasAPI.Model.User;
import com.example.mascotasAPI.Service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
@RequestMapping("/users")
@Api(value = "Api de usuarios", description = "Métodos para gestiónar los usuarios")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "Obtener todos", notes = "EndPoint para obtener todos los usuarios")
    @GetMapping(value = "/")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getAllUsers() {
        try {
            List<User> lstUser = userService.findAll();
            JSONObject response = new JSONObject();
            response.put("Users", lstUser);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            return new ResponseEntity<String>("se presento un fallo al consultar el servicio", HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Agregar usuario", notes = "EndPoint para agregar un nuevo usuario")
    @PostMapping(value = "/add")
    public ResponseEntity<?> addUsers(@RequestBody User user) {
        JSONObject response = new JSONObject();
        try {
            user.setId("");
            User findUser = userService.findByEmail( user.getEmail() );
            if(findUser == null) {
                userService.saveOrUpdateUser(user);
                response.put("Status", true);
                return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
            }else{
                response.put("Status", false);
                response.put("Data", "El correo electrónico ya se encuentra registrado.");
                return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
            }
        }catch (Exception exc){
            response.put("Status", false);
            response.put("Data", "se presento algun problema, por favor consulte con el administrador");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Eliminar usuario", notes = "EndPoint para eliminar un usuario por su identificador")
    @PostMapping(value = "/delete")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> deleteUsers(@RequestBody String id) {
        try {
            userService.deleteUser(id);
            JSONObject response = new JSONObject();
            response.put("Status", true);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            return new ResponseEntity<String>("se presento algun problema, por favor consulte con el administrador", HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Iniciar Sesión", notes = "EndPoint encargado de generar el token de autenticación")
    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody User user) {
        JSONObject response = new JSONObject();
        try {
            User findUser = userService.findByEmail( user.getEmail() );
            if ( !findUser.getPassword().equals(user.getPassword()) ) {
                response.put("Data", "email or password is wrong");
                response.put("Status", false);
                return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
            }
            else {
                String token = getJWTToken(user.getEmail());
                response.put("Data", token);
                response.put("Status", true);
                response.put("UserId", findUser.getId());
                return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
            }
        }
        catch ( NullPointerException e ) {
            response.put("Data", "email or password is wrong");
            response.put("Status", false);
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    private String getJWTToken(String email) {
        String secretKey = "mySecretKey";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId("softtekJWT")
                .setSubject(email)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 6000000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;
    }
}
