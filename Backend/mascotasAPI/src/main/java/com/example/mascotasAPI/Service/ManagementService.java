package com.example.mascotasAPI.Service;

import com.example.mascotasAPI.Model.Management;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ManagementService {
    Management findByid(String id);
    List<Management> findAll();
    List<Management> findAllByPetId(String petId);
    List<Management> findAllByUserId(String userId);
    void saveOrUpdateManagement(Management management);
    void deleteManagement(String id);
}
