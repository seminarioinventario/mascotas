package com.example.mascotasAPI.Service;

import com.example.mascotasAPI.Model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    List<User> findAll();
    User findByEmail(String email);
    List<User> findAllByOrderByName();
    void saveOrUpdateUser(User user);
    void deleteUser(String id);
}
