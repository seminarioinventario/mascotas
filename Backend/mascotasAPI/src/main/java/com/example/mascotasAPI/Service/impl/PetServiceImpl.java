package com.example.mascotasAPI.Service.impl;

import com.example.mascotasAPI.Model.Pet;
import com.example.mascotasAPI.Repository.PetRepository;
import com.example.mascotasAPI.Service.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PetServiceImpl implements PetService {
    @Autowired
    private PetRepository petRepository;

    @Override
    public List<Pet> findAllByOrderByEstado() {
        return petRepository.findAllByOrderByEstado();
    }

    @Override
    public Pet findByid(String id) {
        return petRepository.findByid(id);
    }

    @Override
    public List<Pet> findAllByUserId(String userId) {
        return petRepository.findAllByUserId(userId);
    }

    @Override
    public List<Pet> findAllByRaza(String raza) {
        return petRepository.findAllByRaza(raza);
    }

    @Override
    public List<Pet> findAllByColor(String color) {
        return petRepository.findAllByColor(color);
    }

    @Override
    public List<Pet> findAllByEdad(String edad) {
        return petRepository.findAllByEdad(edad);
    }

    @Override
    public List<Pet> findAllByEstado(String estado) {
        return petRepository.findAllByEstado(estado);
    }

    @Override
    public List<Pet> findAllByEspecie(String especie) {
        return petRepository.findAllByEstado(especie);
    }

    @Override
    public Pet findAllByName(String name) {
        return petRepository.findAllByName(name);
    }

    @Override
    public void saveOrUpdatePet(Pet pet) {
        petRepository.save(pet);
    }

    @Override
    public void deletePet(String id) {
        petRepository.deleteById(id);
    }
}
