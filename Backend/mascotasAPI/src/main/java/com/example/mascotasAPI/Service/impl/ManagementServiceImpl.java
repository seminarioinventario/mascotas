package com.example.mascotasAPI.Service.impl;

import com.example.mascotasAPI.Model.Management;
import com.example.mascotasAPI.Repository.ManagementRepository;
import com.example.mascotasAPI.Service.ManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ManagementServiceImpl implements ManagementService {

    @Autowired
    private ManagementRepository managementRepository;

    @Override
    public Management findByid(String id) {
        return managementRepository.findByid(id);
    }

    @Override
    public List<Management> findAll() {
        return managementRepository.findAll();
    }

    @Override
    public List<Management> findAllByPetId(String petId) {
        return managementRepository.findAllByPetId(petId);
    }

    @Override
    public List<Management> findAllByUserId(String userId) {
        return managementRepository.findAllByUserId(userId);
    }

    @Override
    public void saveOrUpdateManagement(Management management) {
        managementRepository.save(management);
    }

    @Override
    public void deleteManagement(String id) {
        managementRepository.deleteById(id);
    }
}
