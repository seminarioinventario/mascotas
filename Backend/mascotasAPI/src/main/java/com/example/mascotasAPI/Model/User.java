package com.example.mascotasAPI.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
@Document(collection = "users")
@ApiModel("Modelo de usuario")
public class User {
    @NotNull
    @Id
    @ApiModelProperty(value = "Identificador del usuario", required = true)
    private String id;
    @ApiModelProperty(value = "Nombre del usuario", required = false)
    private String name;
    @NotNull
    @ApiModelProperty(value = "Correo del usuario", required = true)
    private String email;
    @NotNull
    @ApiModelProperty(value = "Contraseña del usuario", required = true)
    private String password;
    @ApiModelProperty(value = "Telefono del usuario", required = false)
    private String cellPhone;

    public User(String id, String name, String email, String password, String cellPhone) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.cellPhone = cellPhone;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCellPhone(){ return cellPhone;}

    public void setCellPhone(String cellPhone){this.cellPhone = cellPhone;}
}
