package com.example.mascotasAPI.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
@Document(collection = "managements")
@ApiModel("Modelo de las gestiones")
public class Management {
    @NotNull
    @Id
    @ApiModelProperty(value = "Identificador de la gestión", required = true)
    private String id;
    @NotNull
    @ApiModelProperty(value = "Fecha de solicitud", required = true)
    private String fecha_solicitud;
    @NotNull
    @ApiModelProperty(value = "Fecha de respuesta", required = true)
    private String fecha_respuesta;
    @ApiModelProperty(value = "Comentario del solicitante", required = false)
    private String comentarios_solicitud;
    @ApiModelProperty(value = "Comentario de la respuesta", required = false)
    private String comentarios_respuestas;
    @ApiModelProperty(value = "Estado de la respuesta (1-aceptada, 2-rechazada)", required = false)
    private String resultado;
    @NotNull
    @ApiModelProperty(value = "Identificación del solicitante", required = true)
    private String userId;
    @NotNull
    @ApiModelProperty(value = "Identificación de la mascota", required = true)
    private String petId;

    public Management() {
    }

    public Management(String id, String fecha_solicitud, String fecha_respuesta, String comentarios_solicitud,
                      String comentarios_respuestas, String resultado, String user, String pet) {
        this.id = id;
        this.fecha_solicitud = fecha_solicitud;
        this.fecha_respuesta = fecha_respuesta;
        this.comentarios_solicitud = comentarios_solicitud;
        this.comentarios_respuestas = comentarios_respuestas;
        this.resultado = resultado;
        this.userId = user;
        this.petId = pet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFecha_solicitud() {
        return fecha_solicitud;
    }

    public void setFecha_solicitud(String fecha_solicitud) {
        this.fecha_solicitud = fecha_solicitud;
    }

    public String getFecha_respuesta() {
        return fecha_respuesta;
    }

    public void setFecha_respuesta(String fecha_respuesta) {
        this.fecha_respuesta = fecha_respuesta;
    }

    public String getComentarios_solicitud() {
        return comentarios_solicitud;
    }

    public void setComentarios_solicitud(String comentarios_solicitud) {
        this.comentarios_solicitud = comentarios_solicitud;
    }

    public String getComentarios_respuestas() {
        return comentarios_respuestas;
    }

    public void setComentarios_respuestas(String comentarios_respuestas) {
        this.comentarios_respuestas = comentarios_respuestas;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPetId() {
        return petId;
    }

    public void setPetId(String petId) {
        this.petId = petId;
    }
}
