package com.example.mascotasAPI.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
@Document(collection = "pets")
@ApiModel("Modelo de mascotas")
public class Pet {
    @NotNull
    @Id
    @ApiModelProperty(value = "Identificación de la mascota", required = true)
    private String id;
    @ApiModelProperty(value = "Nombre de la mascota", required = false)
    private String name;
    @NotNull
    @ApiModelProperty(value = "Raza de la mascota", required = true)
    private String raza;
    @NotNull
    @ApiModelProperty(value = "Especie de la mascota", required = true)
    private String especie;
    @NotNull
    @ApiModelProperty(value = "Edad de la mascota", required = true)
    private String edad;
    @ApiModelProperty(value = "Estatura de la mascota", required = false)
    private String estatura;
    @ApiModelProperty(value = "Color de la mascota", required = false)
    private String color;
    @NotNull
    @ApiModelProperty(value = "Estado de la mascota (1-Adoptada, 2. En adopción)", required = true)
    private String estado;
    @ApiModelProperty(value = "Comentario de la mascota", required = false)
    private String comentarios;
    @ApiModelProperty(value = "Foto de la mascota", required = false)
    private String imagen;
    @NotNull
    @ApiModelProperty(value = "Identificación del dueño", required = true)
    private String userId;

    public Pet(String id, String name, String raza, String especie, String edad, String estatura, String color, String estado,
               String comentarios, String imagen, String userId) {
        this.id = id;
        this.name = name;
        this.raza = raza;
        this.edad = edad;
        this.estatura = estatura;
        this.color = color;
        this.estado = estado;
        this.comentarios = comentarios;
        this.userId = userId;
        this.especie=especie;
        this.imagen=imagen;
    }

    public Pet() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getEstatura() {
        return estatura;
    }

    public void setEstatura(String estatura) {
        this.estatura = estatura;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
