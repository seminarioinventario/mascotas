package com.example.mascotasAPI.Controller;

import com.example.mascotasAPI.Model.Management;
import com.example.mascotasAPI.Service.ManagementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
@RequestMapping("/managements")
@Api(value = "Api para las gestiones", description = "Métodos para controlar las gestiones de mascotas")
public class ManagementController {
    @Autowired
    private ManagementService managementService;

    @ApiOperation(value = "Obtener todas las getsiones", notes = "EndPoint encargado de traer todas las gestiones del sistema")
    @GetMapping(value = "/")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getAll() {
        JSONObject response = new JSONObject();
        try {
            List<Management> lstManag = managementService.findAll();
            response.put("Status", true);
            response.put("Data", lstManag);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", true);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Buscar gestión por mascota", notes = "EndPoint encargado de buscar todas las gestiones por el identificador de una mascota")
    @GetMapping(value = "/findByPet")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getFindByPetId(@RequestBody String petId) {
        JSONObject response = new JSONObject();
        try {
            List<Management> lstManag = managementService.findAllByPetId(petId);
            response.put("Status", true);
            response.put("Data", lstManag);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", true);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Buscar gestión por solicitante", notes = "EndPoint encargado de buscar todas las gestiones por el identificador de un solicitante")
    @GetMapping(value = "/findByUser")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getFindByUserId(@RequestBody String userId) {
        JSONObject response = new JSONObject();
        try {
            List<Management> lstManag = managementService.findAllByUserId(userId);
            response.put("Status", true);
            response.put("Data", lstManag);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", true);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Buscar gestión", notes = "EndPoint encargado de buscar una gestión por su identificador")
    @GetMapping(value = "/findById")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getManagement(@RequestBody String id) {
        JSONObject response = new JSONObject();
        try {
            Management manag = managementService.findByid(id);
            response.put("Status", true);
            response.put("Data", manag);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", true);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Agregar gestión", notes = "EndPoint encargado de agregar una gestión")
    @PostMapping(value = "/add")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> addManagement(@RequestBody Management management) {
        JSONObject response = new JSONObject();
        try {
            managementService.saveOrUpdateManagement(management);
            response.put("Status", true);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", true);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Eliminar gestión", notes = "EndPoint encargado de eliminar una gestión por su identificador")
    @PostMapping(value = "/delete")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> deleteManagement(@RequestBody String id) {
        JSONObject response = new JSONObject();
        try {
            managementService.deleteManagement(id);
            response.put("Status", true);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", true);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

}
