package com.example.mascotasAPI.Service;

import com.example.mascotasAPI.Model.Pet;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PetService {
    List<Pet> findAllByOrderByEstado();
    Pet findByid(String id);
    List<Pet> findAllByUserId(String userId);
    List<Pet> findAllByRaza(String raza);
    List<Pet> findAllByColor(String color);
    List<Pet> findAllByEdad(String edad);
    List<Pet> findAllByEstado(String estado);
    List<Pet> findAllByEspecie(String especie);
    Pet findAllByName(String name);
    void saveOrUpdatePet(Pet pet);
    void deletePet(String id);
}
