package com.example.mascotasAPI.Repository;

import com.example.mascotasAPI.Model.Pet;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PetRepository extends MongoRepository<Pet,String> {
    List<Pet> findAllByOrderByEstado();
    Pet findByid(String id);
    List<Pet> findAllByUserId(String userId);
    List<Pet> findAllByRaza(String raza);
    List<Pet> findAllByColor(String color);
    List<Pet> findAllByEdad(String edad);
    List<Pet> findAllByEstado(String estado);
    Pet findAllByName(String name);
    List<Pet> findAllByEspecie(String especie);
}
