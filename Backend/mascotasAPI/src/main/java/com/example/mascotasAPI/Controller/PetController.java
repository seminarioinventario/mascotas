package com.example.mascotasAPI.Controller;

import com.example.mascotasAPI.Model.Pet;
import com.example.mascotasAPI.Service.PetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
@RequestMapping("/pets")
@Api(value = "Api de mascotas", description = "Métodos para gestiónar las mascotas")
public class PetController {
    @Autowired
    private PetService petService;

    @ApiOperation(value = "Obtener todas las mascotas", notes = "EndPoint encargado de obtener el listado de mascotas adoptables")
    @GetMapping(value = "/")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getAllPets() {
        JSONObject response = new JSONObject();
        try {
            List<Pet> lstPet = petService.findAllByEstado("2");
            response.put("Status", true);
            response.put("Data", lstPet);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", false);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Buscar mascota", notes = "EndPoint encargado buscar de una mascota por su identificador")
    @GetMapping(value = "/findById")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getPetById(@RequestBody String id) {
        JSONObject response = new JSONObject();
        try {
            Pet objPet = petService.findByid(id);
            response.put("Status", true);
            response.put("Data", objPet);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", false);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Buscar mascotas de un usuario", notes = "EndPoint encargado de buscar las mascotas por el identificador del dueño")
    @GetMapping(value = "/findByUser/{id}")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getAllPetsByUserId(@PathVariable("id") String userId ) {
        JSONObject response = new JSONObject();
        try {
            List<Pet> lstPet = petService.findAllByUserId(userId);
            response.put("Status", true);
            response.put("Data", lstPet);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", false);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Buscar mascotas por raza", notes = "EndPoint encargado buscar las mascotas de una raza determinada")
    @GetMapping(value = "/findByRaza")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getAllPetsByRaza(@RequestBody String raza) {
        JSONObject response = new JSONObject();
        try {
            List<Pet> lstPet = petService.findAllByRaza(raza);
            response.put("Status", true);
            response.put("Data", lstPet);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", false);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Buscar mascotas por color", notes = "EndPoint encargado buscar las mascotas de un mismo color")
    @GetMapping(value = "/findByColor")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getAllPetsByColor(@RequestBody String color) {
        JSONObject response = new JSONObject();
        try {
            List<Pet> lstPet = petService.findAllByColor(color);
            response.put("Status", true);
            response.put("Data", lstPet);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", false);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Buscar mascotas por edad", notes = "EndPoint encargado buscar las mascotas de una misma edad")
    @GetMapping(value = "/findByEdad")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getAllPetsByEdad(@RequestBody String edad) {
        JSONObject response = new JSONObject();
        try {
            List<Pet> lstPet = petService.findAllByEdad(edad);
            response.put("Status", true);
            response.put("Data", lstPet);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", false);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Buscar mascotas por estado", notes = "EndPoint encargado buscar las mascotas que se encuentren en un determinado estado")
    @GetMapping(value = "/findByEstado")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getAllPetsByEstado(@RequestBody String estado) {
        JSONObject response = new JSONObject();
        try {
            List<Pet> lstPet = petService.findAllByEstado(estado);
            response.put("Status", true);
            response.put("Data", lstPet);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", false);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Buscar mascotas por especie", notes = "EndPoint encargado buscar las mascotas de una misma especie")
    @GetMapping(value = "/findByEspecie")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> getAllPetsByEspecie(@RequestBody String especie) {
        JSONObject response = new JSONObject();
        try {
            List<Pet> lstPet = petService.findAllByEspecie(especie);
            response.put("Status", true);
            response.put("Data", lstPet);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", false);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Adoptar Mascota", notes = "EndPoint encargado de adoptar la amscota")
    @PostMapping(value = "/takePet")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> AdoptarPet(@RequestBody Map<String, String> json) {
        JSONObject response = new JSONObject();
        try {
            if(json.get("petName") != "") {
                Pet objPet = petService.findAllByName(json.get("petName").toString());
                objPet.setUserId(json.get("userId").toString());
                objPet.setEstado("1");
                petService.saveOrUpdatePet(objPet);
                response.put("Status", true);
                return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
            }else{
                response.put("Status", false);
                response.put("Data", "Datos incompletos.");
                return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
            }
        }catch (Exception exc){
            response.put("Status", false);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Agregar mascota", notes = "EndPoint encargado de agregar una mascota nueva")
    @PostMapping(value = "/add")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> addPet(@RequestBody Pet pet) {
        JSONObject response = new JSONObject();
        try {
            if(pet.getUserId() != "") {
                petService.saveOrUpdatePet(pet);
                response.put("Status", true);
                return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
            }else{
                response.put("Status", false);
                response.put("Data", "Datos incompletos.");
                return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
            }
        }catch (Exception exc){
            response.put("Status", false);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Eliminar mascota", notes = "EndPoint encargado de eliminar una mascota por su identificador")
    @PostMapping(value = "/delete")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    public ResponseEntity<?> deletePet(@RequestBody String id) {
        JSONObject response = new JSONObject();
        try {
            petService.deletePet(id);
            response.put("Status", true);
            return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
        }catch (Exception exc){
            response.put("Status", false);
            response.put("Data", "se presento un fallo al consultar el servicio");
            return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
        }
    }

}
